package Chantier_Barda;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;
import javax.servlet.ServletInputStream;

import org.apache.http.entity.StringEntity;

public class queryhttp {
	
	public static void main(String[] args) throws IOException{
		
		URL api = new URL("https://gnomad.broadinstitute.org/api2");
		HttpsURLConnection connection =(HttpsURLConnection) api.openConnection();
		String request =new String("{'query':'{\n    gene(gene_id: \'ENSG00000169174\') {\n      clinvar_variants {\n        alleleId\n        clinicalSignificance\n        goldStars\n        majorConsequence\n        pos\n        variantId\n      }\n      variants(dataset: gnomad_r2_1) {\n        consequence\n        isCanon: consequence_in_canonical_transcript\n        flags\n        hgvs\n        hgvsc\n        hgvsp\n        pos\n        rsid\n        variant_id: variantId\n        xpos\n        exome {\n          ac\n          ac_hemi\n          ac_hom\n          an\n          af\n          filters\n          populations {\n            id\n            ac\n            an\n            ac_hemi\n            ac_hom\n          }\n        }\n        genome {\n          ac\n          ac_hemi\n          ac_hom\n          an\n          af\n          filters\n          populations {\n            id\n            ac\n            an\n            ac_hemi\n            ac_hom\n          }\n        }\n      }\n    }\n  }','variables':{}}");
		try{
		connection.setRequestMethod("POST");
	    connection.setRequestProperty("Content-Type", "application/json");
	    connection.setRequestProperty("Content-Length", 
	    Integer.toString(request.getBytes().length));
		connection.setRequestProperty("Content-Language", "en-US");  

		connection.setUseCaches(false);
		connection.setDoOutput(true);

			    //Send request
	    DataOutputStream wr = new DataOutputStream (
	    connection.getOutputStream());
			    wr.writeBytes(request);
			    wr.close();

			    //Get Response
		        PrintWriter file = new PrintWriter("C:\\Users\\domec\\Desktop\\genetonic\\GeneTonic\\temp\\PCSK9.json");

			    InputStream is = connection.getInputStream();
			    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
			    StringBuilder response = new StringBuilder(); // or StringBuffer if Java version 5+
			    String line;
			    while ((line = rd.readLine()) != null) {
			      response.append(line);
			      response.append('\r');
			      System.out.println(line);
			    }
			    file.write(response.toString());
			    rd.close();
			   // return response.toString();
			  } catch (Exception e) {
			    e.printStackTrace();
			    //return null;
			  } finally {
			    if (connection != null) {
			      connection.disconnect();
			    }
			  }
			}
}

