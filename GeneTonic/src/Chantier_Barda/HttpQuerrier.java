package Chantier_Barda;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;



import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.openqa.selenium.json.Json;





public class HttpQuerrier {
	private String requestFirstPart = "https://gnomad.broadinstitute.org/api?query=query%7B%0Agene(gene_id%3A%20%22" ;
	private String requestSecondPart = "%22)%20%7B%20%20%20%20%20%20%0A%20%20clinvar_variants%20%7B%20%20%20%20%20%20%20%20alleleId%20%20%20%20%20%20%20%20clinicalSignificance%20%20%20%20%20%20%20%20goldStars%20%20%20%20%20%20%20%20majorConsequence%20%20%20%20%20%0A%20%20%20%20pos%20%20%20%20%20%20%20%20variantId%20%20%20%20%20%20%7D%20%20%20%20%0A%20%20variants(dataset%3A%20gnomad_r2_1)%20%7B%20%20%20%20%0A%20%20%20%20consequence%20%20%20%20%20%20%20%20isCanon%3A%20consequence_in_canonical_transcript%20%20%20%20%0A%20%20%20%20flags%20%20%20%20%20%20%20%20hgvs%20%20%20%20%20%20%20%20hgvsc%20%20%20%20%20%20%20%20hgvsp%20%20%20%20%20%20%20%20pos%20%20%20%20%20%20%20%20rsid%20%20%20%20%20%20%20%0A%20%20%20%20variant_id%3A%20variantId%20%20%20%20%20%20%20%20xpos%20%20%20%20%20%20%20%20exome%20%7B%20%20%20%20%20%20%20%20%20%20ac%20%20%20%20%20%20%20%20%20%20ac_hemi%20%20%20%20%20%20%20%20%20%0A%20%20%20%20%20%20ac_hom%20%20%20%20%20%20%20%20%20%20an%20%20%20%20%20%20%20%20%20%20af%20%20%20%20%20%20%20%20%20%20filters%20%20%20%20%20%20%20%20%20%20populations%20%7B%20%20%20%20%20%20%20%20%20%20%20%20id%20%20%20%20%20%20%20%20%20%20%0A%20%20%20%20%20%20%20%20ac%20%20%20%20%20%20%20%20%20%20%20%20an%20%20%20%20%20%20%20%20%20%20%20%20ac_hemi%20%20%20%20%20%20%20%20%20%20%20%20ac_hom%20%20%20%20%20%20%20%20%20%20%7D%20%20%20%20%20%20%20%20%7D%20%20%20%20%20%20%20%20genome%20%7B%20%0A%20%20%20%20%20%20ac%20%20%20%20%20%20%20%20%20%20ac_hemi%20%20%20%20%20%20%20%20%20%20ac_hom%20%20%20%20%20%20%20%20%20%20an%20%20%20%20%20%20%20%20%20%20af%20%20%20%20%20%20%20%20%20%20filters%20%20%20%20%20%20%20%20%0A%20%20%20%20%20%20populations%20%7B%20%20%20%20%20%20%20%20%20%20%20%20id%20%20%20%20%20%20%20%20%20%20%20%20ac%20%20%20%20%20%20%20%20%20%20%20%20an%20%20%20%20%20%20%20%20%20%20%20%20ac_hemi%20%20%20%20%20%20%20%20%20%20%20%0A%20%20%20%20%20%20%20%20ac_hom%20%20%20%20%20%20%20%20%20%20%7D%20%20%20%20%20%20%20%20%7D%20%20%20%20%20%20%7D%20%20%20%20%7D%20%20%7D";
	private String gene;

	
	HttpQuerrier(){
		}
	
	public void setgene(String gene){
		this.gene = gene;
	}
	
	public String getgene(){
		return this.gene;
		}
	
    public static void main(String[] args) throws IOException {
    	HttpQuerrier myQuerry = new HttpQuerrier();
    	myQuerry.setgene("ENSG00000169174");
    	myQuerry.gnomadRequest();
    }

    public void gnomadRequest(){
        try (CloseableHttpClient client = HttpClientBuilder.create().build()) {
        
            //HttpGet request = new HttpGet("https://gnomad.broadinstitute.org/api?query=query%7B%0Agene(gene_id%3A%20%22ENSG00000169174%22)%20%7B%20%20%20%20%20%20%0A%20%20clinvar_variants%20%7B%20%20%20%20%20%20%20%20alleleId%20%20%20%20%20%20%20%20clinicalSignificance%20%20%20%20%20%20%20%20goldStars%20%20%20%20%20%20%20%20majorConsequence%20%20%20%20%20%0A%20%20%20%20pos%20%20%20%20%20%20%20%20variantId%20%20%20%20%20%20%7D%20%20%20%20%0A%20%20variants(dataset%3A%20gnomad_r2_1)%20%7B%20%20%20%20%0A%20%20%20%20consequence%20%20%20%20%20%20%20%20isCanon%3A%20consequence_in_canonical_transcript%20%20%20%20%0A%20%20%20%20flags%20%20%20%20%20%20%20%20hgvs%20%20%20%20%20%20%20%20hgvsc%20%20%20%20%20%20%20%20hgvsp%20%20%20%20%20%20%20%20pos%20%20%20%20%20%20%20%20rsid%20%20%20%20%20%20%20%0A%20%20%20%20variant_id%3A%20variantId%20%20%20%20%20%20%20%20xpos%20%20%20%20%20%20%20%20exome%20%7B%20%20%20%20%20%20%20%20%20%20ac%20%20%20%20%20%20%20%20%20%20ac_hemi%20%20%20%20%20%20%20%20%20%0A%20%20%20%20%20%20ac_hom%20%20%20%20%20%20%20%20%20%20an%20%20%20%20%20%20%20%20%20%20af%20%20%20%20%20%20%20%20%20%20filters%20%20%20%20%20%20%20%20%20%20populations%20%7B%20%20%20%20%20%20%20%20%20%20%20%20id%20%20%20%20%20%20%20%20%20%20%0A%20%20%20%20%20%20%20%20ac%20%20%20%20%20%20%20%20%20%20%20%20an%20%20%20%20%20%20%20%20%20%20%20%20ac_hemi%20%20%20%20%20%20%20%20%20%20%20%20ac_hom%20%20%20%20%20%20%20%20%20%20%7D%20%20%20%20%20%20%20%20%7D%20%20%20%20%20%20%20%20genome%20%7B%20%0A%20%20%20%20%20%20ac%20%20%20%20%20%20%20%20%20%20ac_hemi%20%20%20%20%20%20%20%20%20%20ac_hom%20%20%20%20%20%20%20%20%20%20an%20%20%20%20%20%20%20%20%20%20af%20%20%20%20%20%20%20%20%20%20filters%20%20%20%20%20%20%20%20%0A%20%20%20%20%20%20populations%20%7B%20%20%20%20%20%20%20%20%20%20%20%20id%20%20%20%20%20%20%20%20%20%20%20%20ac%20%20%20%20%20%20%20%20%20%20%20%20an%20%20%20%20%20%20%20%20%20%20%20%20ac_hemi%20%20%20%20%20%20%20%20%20%20%20%0A%20%20%20%20%20%20%20%20ac_hom%20%20%20%20%20%20%20%20%20%20%7D%20%20%20%20%20%20%20%20%7D%20%20%20%20%20%20%7D%20%20%20%20%7D%20%20%7D");
            HttpGet request = new HttpGet(requestFirstPart + this.gene + requestSecondPart);

        	HttpResponse response = client.execute(request);

            BufferedReader bufReader = new BufferedReader(new InputStreamReader(
                    response.getEntity().getContent()));

            StringBuilder builder = new StringBuilder();

            String line;

            while ((line = bufReader.readLine()) != null){
                builder.append(line);
                builder.append(System.lineSeparator());
            }
            System.out.println(builder);
            FileWriter filew = new FileWriter("C:\\Users\\domec\\Desktop\\genetonic\\GeneTonic\\temp\\" +this.gene +".json");
            filew.write(builder.toString());
            filew.close();
        } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}