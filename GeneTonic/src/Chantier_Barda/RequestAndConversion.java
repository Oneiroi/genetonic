package Chantier_Barda;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.xerces.xs.StringList;

public class RequestAndConversion {
	

	public static void main(String[] args) throws IOException{
		
		HttpQuerrier testQuerrier = new HttpQuerrier();
		jsontocsv testjsontocsv = new jsontocsv();
		List<String> ensemblIdList = new ArrayList<String>();
		ensemblIdList.add("ENSG00000121410");
		ensemblIdList.add("ENSG00000169047");
		for(String gene : ensemblIdList){
			testQuerrier.setgene(gene);
			testQuerrier.gnomadRequest();
			testjsontocsv.setfileAdress("C:\\Users\\domec\\Desktop\\genetonic\\GeneTonic\\temp\\"+gene+".json");
			testjsontocsv.convert();
			
		}
}
}