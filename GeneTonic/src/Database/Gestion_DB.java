package Database;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Gestion_DB extends JPanel {
    private String title = "Gestion Database";

    //Element pour le premier fichier
    private JLabel texte1 = new JLabel("Lien fichier Gene To Pheno");
    public JLabel lien_fichier_GeneToPheno = new JLabel("");
    public JButton recherche_fichier_GeneToPheno = new JButton("recherche fichier GeneToPheno");

    //Element pour le deuxieme fichier
    private JLabel text2 = new JLabel("Lien fichier Pheno To Gene");

    public JLabel lien_fichier_PhenoToGene = new JLabel("");
    public JButton recherche_fichier_PhenoToGene = new JButton("Recherche fichier PhenoToGene");

    // Trois boutons pour initialiser / use / reset la DB
    public JButton Initialisation = new JButton("Initialisation DataBase");
    public JButton Use_DB = new JButton("Use DB");
    public JButton Reset_DB = new JButton("Reset Database");
    //Et texte informatif
    private JLabel information = new JLabel("Initialise lors du premier lancement , use sinon");
    // Deux Strings Pour les liens
    public String lien1 = "";
    public String lien2 = "";

    //Setter Getter Pour les liens
    public void set_GeneToPheno(String lien) {
        lien1 = lien;
    };
    public String get_GeneToPheno(){
        return lien1;
    }

    public void set_PhenoToGene(String lien) {
        lien2 = lien;
    };
    public String get_PhenoToGene(){
       return lien2;
    };

    public Gestion_DB() {
        this.setLayout(new GridLayout(7, 2, 50, 25));
        this.setVisible(true);

        //Bordure Elements;
        Border blackline = BorderFactory.createLineBorder(Color.black);
        this.Initialisation.setBorder(blackline);
        this.texte1.setBorder(blackline);
        this.text2.setBorder(blackline);
        this.lien_fichier_GeneToPheno.setBorder(blackline);
        this.lien_fichier_PhenoToGene.setBorder(blackline);


        this.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
        //On ajoute les elements dans l'ordre que l'on souhaite
        this.add(texte1);
        texte1.setHorizontalAlignment(0); //Centre le texte
        this.add(Box.createGlue()); //Espacement
        this.add(lien_fichier_GeneToPheno);

        this.add(information);
        this.add(recherche_fichier_GeneToPheno);
        this.add(Initialisation);
        Initialisation.setHorizontalAlignment(0);
        this.add(Box.createGlue());
        this.add(Use_DB);
        //
        this.add(text2);
        text2.setHorizontalAlignment(0); //Centre le texte
        this.add(Box.createGlue());
        this.add(lien_fichier_PhenoToGene);
        this.add(Reset_DB);
        this.add(recherche_fichier_PhenoToGene);
        this.add(Box.createGlue());


        //Action des boutons
        recherche_fichier_GeneToPheno.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser choix = new JFileChooser();
                int retour = choix.showOpenDialog(null);
                if (retour == JFileChooser.APPROVE_OPTION) {
                    // un fichier a été choisi (sortie par OK)
                    // nom du fichier  choisi
                    choix.getSelectedFile().getName();
                    // chemin absolu du fichier choisi;
                    set_GeneToPheno(choix.getSelectedFile().getAbsolutePath());
                    lien_fichier_GeneToPheno.setText(lien1);

                    // Si je prend les 2 fichiers faudra mettre cette methode dans principale
                    //et faire les meme modif que pour fichier 1 (mais j'avais des resultats identiques avec 1 seul fichier)

                }
            }
        });
    }
}
