package Database;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Principale {
	

    public static jdbc1 database = new jdbc1();
    private static JTable table1;
    private static void createAndShowGUI() {

        // Create and set up the window.
        final JFrame frame = new JFrame("J'ai une grande soif");

        // Display the window.
        frame.setSize(800, 600);
        frame.setLocation(400,200);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // set grid layout for the frame
        frame.getContentPane().setLayout(new GridLayout(1, 1));

        JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);

        //Onglet Database
        Gestion_DB DB_panel = new Gestion_DB();
        tabbedPane.addTab("Gestion Database", DB_panel);

        //Action des boutons
        DB_panel.Initialisation.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    database.Launch();
                }
                catch (Exception ex){}
            }
        });
        DB_panel.recherche_fichier_GeneToPheno.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                database.setAdresse_file_GeneToPheno(DB_panel.get_GeneToPheno());
                System.out.println(database.getAdresse_file_GeneToPheno());
            }
        });
        DB_panel.recherche_fichier_PhenoToGene.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser choix = new JFileChooser();
                int retour = choix.showOpenDialog(null);
                if (retour == JFileChooser.APPROVE_OPTION) {
                    // un fichier a été choisi (sortie par OK)
                    // nom du fichier  choisi
                    choix.getSelectedFile().getName();
                    // chemin absolu du fichier choisi;
                    DB_panel.set_PhenoToGene(choix.getSelectedFile().getAbsolutePath());
                    DB_panel.lien_fichier_PhenoToGene.setText(DB_panel.lien2);
                    database.setAdresse_file_PhenoToGene(DB_panel.get_PhenoToGene());
                }
            }
        });
        DB_panel.Reset_DB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    database.delete();
                }
                catch (Exception ex){}
            }
        });
        DB_panel.Use_DB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    database.use_db();
                }
                catch (Exception ex){}
            }
        });


        //Onglet Requete
        Requete requete_panel = new Requete();
        tabbedPane.addTab("Requete HPO", requete_panel);



        //Overide bouton requete
        Requete.start.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String HPO = Requete.Enter_HPO.getText();
                //Initialise les noms de colonne
                String[] nom_col = {"Gene ID" , "GeneName"};

                //Initialise 2d vide puis essaye d'en creer à partir de la commande sql
                String[][] data=  null;

                try{
                    data = database.select_mat(HPO);
                }
                catch (Exception ex){}

                TableModel newmodel = new DefaultTableModel(data,nom_col);
                table1 = new JTable(newmodel);
                //table1.setModel(newmodel);
                table1.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
                Requete.Scroll.getViewport().add(table1);
                System.out.println("start");
            }
        });

        //On ajoute le tabbedPane dans notre frame
        frame.getContentPane().add(tabbedPane);
        frame.setVisible(true);
    }


    public static void main(String[] args) throws ClassNotFoundException {
                createAndShowGUI();
                
    }
}
