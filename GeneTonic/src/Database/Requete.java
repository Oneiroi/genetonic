package Database;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;


public class Requete extends JPanel {
    public static JButton start =new JButton("Start");
    public static JTextField Enter_HPO = new JTextField("");
    private JLabel Info = new JLabel("Entrer le terme HPO ci-dessous");
    public static JScrollPane Scroll = new JScrollPane();

    private JPanel Gauche = new JPanel();

    public Requete(){
        this.setLayout(new GridLayout(1,2));

        Gauche.setLayout(new GridLayout(3,1));
        Gauche.add(Info);
        Info.setHorizontalAlignment(0); //Centre le texte
        Gauche.add(Enter_HPO);
        Enter_HPO.setHorizontalAlignment(0);
        Gauche.add(start);
        this.add(Gauche);

        this.add(Scroll);
        Scroll.setBorder(new EmptyBorder(10,10,10,10));
    }
}
