package Database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/*
    ATTENTION :
    Avant que la classe soit fonctionelle, il va falloir integrer le JBDC au projet
    (normalement il est integre au projet, dans le fichier 'lib' mais si c'est pas le cas, il est telechargable sur
    https://dev.mysql.com/downloads/connector/j/

    Ensuite il faut integrer le fichier JAR a son class PATH.
    Sur eclipse, je sais pas comment ca marche, mais j'ai trouve ca :
    https://www.vogella.com/tutorials/Eclipse/article.html#using-jars-libraries-in-eclipse

    Si tout se passe bien, la classe devrait fonctionner sur tout type de base de donnée.
    cf. la classe Main() qui contient un exemple avec la table 'events' du micro_projet et qui devrait en toute logique
    vous chier l'ensemble des rows sur la console.
*/



public class SQLConnector {

    private Connection          connect             = null;
    private Statement           statement           = null;
    private PreparedStatement   preparedStatement   = null;
    private ResultSet           resultSet           = null;

    private String              ip                  = null;
    private String              user                = null;
    private String              password            = null;
    private String              database            = null;


    /*Constructeur*/
    public SQLConnector(String ip, String user, String password, String database) throws Exception{

        this.ip         = ip        ;
        this.user       = user      ;
        this.password   = password  ;
        this.database   = database  ;

        try {
            //Chargement du serveur SQL.
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Oblige de rajouter un string supplémentaire (serverDateTimeErr) pour gérer une erreur de...fuseau horaire??
            //Va falloir trouver une explication // solution.
            //+ il va bien falloir trouver une solution pour ne pas avoir à gérer le password de l'utilisateur en "dur"
            //et également faire en sorte de pouvoir créer une db de toute pièce depuis le GUI (?)

            //Connection à la vase de donnees
            String serverDateTimeErr = "zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
            connect = DriverManager.getConnection("jdbc:mysql://" + ip + "/" + database + "?" + serverDateTimeErr, user, password);

            //Creation d'un statement vide pour faire des query.
            statement = connect.createStatement();
        }
        catch (Exception e){
            throw e;
        }

    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /*Getters*/
    public Connection           getConnect()            {return connect;}
    public Statement            getStatement()          {return statement;}
    public PreparedStatement    getPreparedStatement()  {return preparedStatement;}
    public ResultSet            getResultSet()          {return resultSet;}
    public String               getIp()                 {return ip;}
    public String               getUser()               {return user;}
    public String               getPassword()           {return password;}
    public String               getDatabase()           {return database;}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /*Setters*/
    public void setConnect              (Connection connect)                    {this.connect           = connect;}
    public void setStatement            (Statement statement)                   {this.statement         = statement;}
    public void setPreparedStatement    (PreparedStatement preparedStatement)   {this.preparedStatement = preparedStatement;}
    public void setResultSet            (ResultSet resultSet)                   {this.resultSet         = resultSet;}
    public void setIp                   (String ip)                             {this.ip                = ip;}
    public void setUser                 (String user)                           {this.user              = user;}
    public void setPassword             (String password)                       {this.password          = password;}
    public void setDatabase             (String database)                       {this.database          = database;}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /*Methods*/

    public ResultSet getAllrows(String table) throws Exception {
        //Un petit exemple de query type "SELECT * FROM 'Table'"
        String query = "SELECT * FROM "+this.getDatabase()+"."+table;
        try{
            ResultSet output = statement.executeQuery(query);
            return output;
        }
        catch (Exception e){
            throw e;
        }
    }

    public void selectAll(String table) throws Exception{
        //Un petit exemple de query type "SELECT * FROM 'Table'"


        String query = "SELECT * FROM "+this.getDatabase()+"."+table;
        try{
            resultSet = statement.executeQuery(query);
            writeResultSet(resultSet);
        }
        catch (Exception e){
            throw e;
        }
    }

    private void writeResultSet(ResultSet resultSet) throws SQLException {
        //Methode privee utilisee pour printer toutes les lignes obtenues
        //avec un objet Statement ou PreparedStatement

        while (resultSet.next()) {
            //ATTENTION : SQL fait son interessant et a decide de commencer ses index a 1...on est oblige de decaler.
            //d'ou le +1 en fin de columnCount et le 'int=1' dans la boucle for.
            int columnCount= resultSet.getMetaData().getColumnCount()+1;

            for (int i=1 ; i<columnCount;i++){
                String columnName = resultSet.getMetaData().getColumnName(i);
                String columnValue = resultSet.getString(i);
                System.out.println(columnName+" : "+columnValue);
            }
        System.out.println("----------------------------------------------------");
        }
    }

    // Reinitialise les primitives si elles sont vides. ça marchais si je le mettais pas...
    // Me demandez pas pourquoi...
    private void close() throws Exception {
        try {
            if (resultSet != null) {resultSet.close();}

            if (statement != null) {statement.close();}

            if (connect != null) {connect.close();}
        }catch(Exception e){
            throw e;
        }
    }
}

