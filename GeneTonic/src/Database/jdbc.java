package Database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class jdbc {
// ya 2 string en début la dessous à cause de probleme de time_zone
// au début la 1er marchait mais ensuite seulement la 2e ou tu gères ce probleme.
    //final static String DATABASE_URL = "jdbc:mysql://localhost/";
    final static String DATABASE_URL ="jdbc:mysql://localhost/";


    static Connection connection = null;
    static Statement statement = null;
    static String DATBAS = "kunta_db";

    static String nom_1er_table = "table1";
    static String details_tab1= "HPOID varchar(15) , HPOName varchar(60) , GeneID int(10) , GeneName varchar(10)";

    //Pensez à changer cette addresse en fonction de la vous ou mettez votre dossier

    static String adresse_file = "C:/Users/domec/Downloads/ALL_SOURCES_ALL_FREQUENCIES_phenotype_to_genes.txt";
    static String reste_command = "fields terminated by '\\t' lines terminated by '\\n' IGNORE 1 Lines;";
    private static void createDatabase(String nom_db) throws Exception {
        String sql_stmt = "CREATE DATABASE IF NOT EXISTS  `";
        sql_stmt+=nom_db;
        sql_stmt+="`;";

        statement = connection.createStatement();

        statement.executeUpdate(sql_stmt);

        System.out.println( nom_db + " has successfully been created");
    }
    private static void createTable(String nom_table , String reste) throws Exception {
        String sql_stmt = "CREATE Table if NOT EXISTS `";
        sql_stmt+= nom_table;
        sql_stmt+="` ";
        sql_stmt+= " (" + reste + ");";
        //System.out.println(sql_stmt);

        statement = connection.createStatement();

        statement.executeUpdate(sql_stmt);
    }
    private static void remplir_db(String table_vise, String adresse_file , String command)throws Exception{
        String sql = "load data local infile '" + adresse_file + "' into table " + table_vise + " " +command ;
        System.out.println(sql);
        statement=connection.createStatement();
        statement.executeUpdate(sql);
    }
    private static void use_db(String nom_db)throws Exception{
        String sql_useDB = "USE ";
        sql_useDB+=nom_db;
        statement = connection.createStatement();

        statement.executeUpdate(sql_useDB);
    }
    public static void main(String[] args) {
        try {
            DriverManager.setLoginTimeout(23);
            connection = DriverManager.getConnection(DATABASE_URL, "root", "root");

            createDatabase(DATBAS);
            use_db(DATBAS);

            createTable(nom_1er_table , details_tab1);
            remplir_db(nom_1er_table,adresse_file, reste_command);
        } catch (Exception ex) {
            System.out.println("The following exception has occured: " + ex.getMessage());
        } finally {

            try {
                if (connection != null) {
                    connection.close();
                }
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception ex) {
                System.out.println("The following exception has occured: " + ex.getMessage());
            }
        }
    }
}