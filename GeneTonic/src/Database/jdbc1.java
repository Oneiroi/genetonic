package Database;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class jdbc1 {
    //C'est long mais permet de gerer les problemes de time Zone
    static String DATBAS = "HPO_GENE";
    final static String DATABASE_URL = "jdbc:mysql://localhost/?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";


    static Connection connection = null;
    static Statement statement = null;

    //Variables Pour la 1er table
    static String nom_1er_table = "table1";
    static String details_tab1= "HPOID varchar(15) , HPOName varchar(110) , GeneID int(10) , GeneName varchar(10)";
    static String nom_col_tb1 = "HPOID , HPONAME , GeneID , GeneName";

    //

    static String adresse_file_PhenoToGene = "";
    static String reste_command = "fields terminated by '\\t' lines terminated by '\\n' IGNORE 1 Lines;"; //ça c'etait pour load data mais j'ai pas reussi pour le moment


    // 2e tab (qu'on utilise pas pour le moment)  (ya des restes d'ailleurs dans le code)
    static String nom_2e_tab = "table2";
    static String details_tab2="GeneID varchar(10) , GeneName varchar(10) , HPOName varchar(110), HPOID varchar(10)";
    static String Nom_col_tb2= "GeneID , GeneName , HPOName , HPOID";
    static String adresse_file_GeneToPheno ="C:\\Users\\quent\\Desktop\\Genetonic\\ALL_SOURCES_ALL_FREQUENCIES_genes_to_phenotype.txt";

    //Setter Getter Pour les liens
    public void setAdresse_file_GeneToPheno(String lien){
        adresse_file_GeneToPheno=lien;
    }
    public String getAdresse_file_GeneToPheno(){
        return adresse_file_GeneToPheno;
    }
    public void setAdresse_file_PhenoToGene(String lien){
        adresse_file_PhenoToGene=lien;
    }
    public String getAdresse_file_PhenoToGene(){
        return adresse_file_PhenoToGene;
    }

    public jdbc1(){
        try {
            DriverManager.setLoginTimeout(23);
            connection = DriverManager.getConnection(DATABASE_URL, "root", "root");


        } catch (Exception ex) {
            System.out.println("The following exception has occured: " + ex.getMessage());
        }
    }
    public static void Launch()throws Exception{
        createDatabase(DATBAS);
        use_db();
        createTable(nom_1er_table,details_tab1);
        //createTable(nom_2e_tab,details_tab2);  //(toujours 2e table)
        remplir_Insert(nom_1er_table,adresse_file_PhenoToGene,nom_col_tb1);
        //remplir_Insert(nom_2e_tab,adresse_file_GeneToPheno,Nom_col_tb2); //2e table
    }
    private static void createDatabase(String nom_db) throws Exception {
        String sql_stmt = "CREATE DATABASE IF NOT EXISTS  `";
        sql_stmt+=nom_db;
        sql_stmt+="`;";

        statement = connection.createStatement();

        statement.executeUpdate(sql_stmt);

        System.out.println( nom_db + " has successfully been created or already created");
    }
    private static void createTable(String nom_table , String reste) throws Exception {
        String sql_stmt = "CREATE Table if NOT EXISTS `";
        sql_stmt+= nom_table;
        sql_stmt+="` ";
        sql_stmt+= " (" + reste + ");";
        //System.out.println(sql_stmt);

        statement = connection.createStatement();

        statement.executeUpdate(sql_stmt);
        System.out.println("Table successfully created or already created");
    }
    public static void use_db()throws Exception{
        String sql_useDB = "USE ";
        sql_useDB+=DATBAS;
        statement = connection.createStatement();
        statement.executeUpdate(sql_useDB);
        System.out.println("Use "+ DATBAS);
    }
    public static void delete()throws Exception{
        String sql_delete = "drop database " + DATBAS + ";";
        //System.out.println( "====> " +sql_delete);
        statement = connection.createStatement();
        statement.executeUpdate(sql_delete);
        System.out.println(DATBAS + " deleted");
    }

    private static void remplir_Insert(String table_vise , String adresse_file , String nom_col) throws Exception{
        String fileName = adresse_file;

        // This will reference one line at a time
        String line = null;

        try {
            // FileReader reads text files in the default encoding.
            FileReader fileReader =
                    new FileReader(fileName);

            // Always wrap FileReader in BufferedReader.
            BufferedReader bufferedReader =
                    new BufferedReader(fileReader);
            int i =0;
            while((line = bufferedReader.readLine()) != null ) {
                if(i>=1) {
                    String[] parts = line.split("\\t");
                    String part1 = parts[0];
                    String part2 = parts[1];
                    String part3 = parts[2];
                    String part4 = parts[3];
                    String sql = "INSERT INTO " + table_vise + " (" + nom_col + ") ";
                    sql += " VALUES (" + " \"" + part1 + "\", \"" + part2 + "\", " + "\""+ part3 + "\", "+ " \"" + part4 + "\");";
                    System.out.println(sql);
                    statement = connection.createStatement();
                    statement.executeUpdate(sql);
                    System.out.println("envoie");
                }
                i++;
            }

            // Always close files.
            bufferedReader.close();
        }
        catch(FileNotFoundException ex) {
            System.out.println(
                    "Unable to open file '" +
                            fileName + "'");
        }
        catch(IOException ex) {
            System.out.println(
                    "Error reading file '"
                            + fileName + "'");
        }
        System.out.println("Table Remplie");
    }


    public static String[][] select_mat(String HPO_name ) throws Exception{
        String sql ="SELECT GeneID , GeneName FROM table1 WHERE HPOID=" + "\"" + HPO_name + "\"" + ";";
        //System.out.println(sql);  //verification de la commande
        statement = connection.createStatement();
        ResultSet RS = statement.executeQuery(sql);

        //Recupere la taille de la commande pour initialiser la matrice
        int taille_requete=0;
        if(RS.last()){
            taille_requete=RS.getRow();
            RS.beforeFirst();
        }
        String[][] data = new String[taille_requete][2];

        int row =0;
        while (RS.next()){
            String s = RS.getString("GeneID");
            String n = RS.getString("GeneName");
            data[row][0]=s;
            data[row][1]=n;
            //Ya peutet moyen de faire ça en plus joli
            //mais ça march
            row++;
        }
        return data;
    }


    //ça c'est pour si on veut utiliser les 2 fichiers (on verra si c'est utile) et à modifier pour 2d
    public static String select_gene_from_HPO_v2(String HPO_ID) throws Exception{
        String sql ="SELECT GeneID, GeneName From table1 where HPOID=\"" +HPO_ID +"\"";
        sql+= " UNION SELECT GeneID, GeneName From table2 where HPOID=\"" +HPO_ID +"\" ORDER BY GeneName asc";
        //System.out.println(sql);    //Verif de la commande
        statement = connection.createStatement();
        ResultSet RS = statement.executeQuery(sql);
        String result ="";
        while (RS.next()){
            int s = RS.getInt("GeneID");
            String n = RS.getString("GeneName");
            result+=n;
            System.out.println(s + "\t" + n);
        }
        return result;
    }

}
