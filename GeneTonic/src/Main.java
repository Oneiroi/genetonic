import Database.SQLConnector;

public class Main {
    public static void main(String[] args) throws Exception {

        String ip       =   "localhost";
        String user     =   "root";
        String password =   "";
        String database =   "projet";
        String table    =   "events";

        SQLConnector test = new SQLConnector(ip,user,password,database);
        test.selectAll(table);
    }
}
